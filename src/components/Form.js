import React from 'react'
// import React, {useState} from 'react';
// import uuid from 'uuid/v1'
import '../App.css';

function Form (props) {

    const [date, distance, onChangeDate, onChangeDistance, onSubmit ] = props;

    return (
        <form onSubmit={onSubmit}>
            <input onChange={onChangeDate} type="date" value={date}/>
            <input onChange={onChangeDistance} type="text" value={distance}/>
            <button>OK</button>
        </form>
        )
}


export default Form;