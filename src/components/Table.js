import React from 'react'
import '../App.css';


function Table (props) {
    const [steps, onDelete, onEdit] = props;
    return (
        <table>
            <thead>
            <tr>
                <th>Дата</th>
                <th>Пройдено км</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            {
                steps.map(
                    ({id, date, distance}, index) => <tr key={index}>
                        <td>{date}</td>
                        <td>{distance}</td>
                        <td>
                            <button onClick={() => onEdit(id)}>Edit</button>
                            <button onClick={() => onDelete(id)}>Delete</button>
                        </td>
                    </tr>
                )
            }
            </tbody>
        </table>
    );
}

export default Table;