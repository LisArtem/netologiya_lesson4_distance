import React, {useState} from 'react';
import uuid from 'uuid/v1'
import '../App.css';

import Form from './Form'
import Table from './Table'

const AddingForm = () => {

    const [id, setId] = useState('');
    const [date, setDate] = useState('');
    const [distance, setDistance] = useState('');
    const [steps, setSteps] = useState([]);

    const onChangeDate = e => {
        setDate(e.target.value);
    };

    const onChangeDistance = e => {
        setDistance(e.target.value);
    };

    const onSubmit = e => {
        e.preventDefault();
        if (!date || !distance) {
            return null;
        }
        const step = steps.find(step => step.id === id);
        if (step) {
            const newSteps = steps.filter(step => step.id !== id);
            setSteps([...newSteps, {id: uuid(), date, distance}]
                .sort((a, b) => a.date > b.date ? 1 : -1));
        } else {
            setSteps([...steps, {id: uuid(), date, distance}]
                .sort((a, b) => a.date > b.date ? 1 : -1));
        }
        setId('');
        setDate('');
        setDistance('');
    };

    const onDelete = id => {
        setSteps(steps.filter(step => step.id !== id));
    };

    const onEdit = id => {
        const step = steps.find(step => step.id === id);
        const {date, distance} = step;
        setId(id);
        setDate(date);
        setDistance(distance);
    };

    return (
        <div className="form-container">
            <Form onChangeDate={onChangeDate} onChangeDistance={onChangeDistance} onSubmit={onSubmit}/>
            <Table steps={steps} onDelete={onDelete} onEdit={onEdit}/>}
        </div>
    )
};


export default AddingForm;